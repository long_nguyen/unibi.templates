
<!-- README.md is generated from README.Rmd. Please edit that file -->

# unibi.templates

<!-- badges: start -->
<!-- badges: end -->

{unibi.templates} provides R Markdown templates for documents and
presentations following Bielefeld University’s corporate Design.

## Installation

You can install the development version of {unibi.templates} like so:

``` r
remotes::install_gitlab(
  "long_nguyen/unibi.templates",
  host = "https://gitlab.ub.uni-bielefeld.de",
  dependencies = TRUE
)
```

## Usage

In RStudio, templates can be selected with **New File → R Markdown… →
From Template**.
